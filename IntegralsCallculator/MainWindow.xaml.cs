﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Drawing;
using System.Globalization;

namespace IntegralsCallculator
{
    public partial class MainWindow : Window
    {
        private double[] _dataX;
        private List<Func<double, double>> _functions;
        private bool _precise;
        private bool _autoClearing;

        private readonly Regex _polynomialPattern = new Regex(@"^[\-\+\s]*[0-9]*(?:\" + NumberFormatInfo.CurrentInfo.NumberDecimalSeparator + @"[0-9]*)?([a-zA-Z])([^-])[\-\+\s]*([0-9])+$");
        private readonly Regex _simplePattern = new Regex(@"^[\-\+\s]*[0-9]*(?:\" + NumberFormatInfo.CurrentInfo.NumberDecimalSeparator + @"[0-9]*)?([a-z])$");
        private readonly Regex _numberPattern = new Regex(@"^[\-\+\s]*[0-9]*(?:\" + NumberFormatInfo.CurrentInfo.NumberDecimalSeparator + @"[0-9]*)?$");
        private readonly Regex _letterPattern = new Regex("^[a-zA-z]$");

        private readonly Random _random = new Random();
        private Color _currentColor;

        public MainWindow()
        {
            InitializeComponent();
          
            MainGraph.plt.Style(ScottPlot.Style.Gray1);
            MainGraph.plt.Legend(location: ScottPlot.legendLocation.lowerLeft);
            MainGraph.Render();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CalculatePolynialFromText(Input.Text);
        }

        private float GetNumberFromInput(string input)
        {
            if(!int.TryParse($"{input[0]}", out _))
            {
                return 1;
            }

            float value;
            if (int.TryParse($"{input.Last()}", out int x))
            {
                if (input[0] == '-' || input[0] == '+')
                {
                    value = Convert.ToSingle(input[1..]);
                }
                else
                {
                    value = Convert.ToSingle(input);
                }
            }
            else
            {
                if (input[0] == '-' || input[0] == '+')
                {
                    value = Convert.ToSingle(input[1..^1]);
                }
                else
                {
                    value = Convert.ToSingle(input[0..^1]);
                }
            }

            if (input[0] == '-')
            {
                value = -value;
            }

            return value;
        }

        private void RenderPlotScatter(double[] x, double[] y, string name, float size = 5, float lineSize = 1, ScottPlot.MarkerShape shape = ScottPlot.MarkerShape.openCircle)
        {
            MainGraph.plt.PlotScatter(x, y, lineWidth: lineSize, markerSize: size, markerShape: shape, color: _currentColor, label: name);
            MainGraph.plt.Axis(-5, 5, -5, 5);
            MainGraph.Render();
        }

        private void RenderStepPlot(double[] x, double[] y, string name, float width = 1)
        {
            MainGraph.plt.PlotStep(x, y, _currentColor, width, name);
            MainGraph.Render();
        }

        private void RenderFinalBorder(double x0, double x1, double y0, double y1) 
        { 
            MainGraph.plt.PlotScatter(new [] { x0, x0 }, new [] { 0d, y0 }, lineWidth: 1, color: _currentColor, markerSize:10, markerShape: ScottPlot.MarkerShape.none, label: "Bounds", lineStyle: ScottPlot.LineStyle.Dash);
            MainGraph.plt.PlotScatter(new [] { x1, x1 }, new [] { 0d, y1 }, lineWidth: 1, color: _currentColor, markerSize: 10, markerShape: ScottPlot.MarkerShape.none, lineStyle: ScottPlot.LineStyle.Dash);
            MainGraph.plt.PlotScatter(new [] { x0, x1 }, new [] { 0d, 0d }, lineWidth: 1, color: _currentColor, markerSize: 10, markerShape: ScottPlot.MarkerShape.none, lineStyle: ScottPlot.LineStyle.Dash);
            MainGraph.Render();
        }

        private void CalculatePolynialFromText(string input)
        {
            if (!int.TryParse(Min.Text, out int min))
            {
                _functions = null;
                return;
            }

            if (!int.TryParse(Max.Text, out int max))
            {
                _functions = null;
                return;
            }

            max += 1;

            if (max <= min) 
            {
                _functions = null;
                return; 
            }

            if (_autoClearing)
            {
                ClearSketch();
            }

            var capacity = Math.Abs(max - min);

            var lineX = new double[capacity];
            var lineY = new double[capacity];

            var text = input.Replace(" ", "");

            if(text[0] == NumberFormatInfo.CurrentInfo.NumberDecimalSeparator[0])
            {
                text = text.Insert(0, "0");
            }

            Input.Text = text;

            var minus = false;

            if (text[0] == '-')
            {
                minus = true;
                
                text = text.Substring(1);
            }

            var segments = text.Split('+', '-');

            var symbols = new List<char>(segments.Length - 1);

            for (int i = 0; i < text.Length; i++)
            {
                if (text[i] == '-' || text[i] == '+')
                {
                    symbols.Add(text[i]);
                }
            }

            var functions = new List<Func<double, double>>();

            for (int i = 0; i < segments.Length; i++)
            {
                var symbol = 1;

                if(i == 0)
                {
                    symbol = minus ? -1 : 1;
                }
                else if(symbols.Count > 0)
                {
                    symbol = i != 0 && symbols[i - 1] == '-' ? -1 : 1;
                }

                var segment = segments[i];

                if(segment == "sin") 
                {
                    functions.Add(j => j = Math.Sin(j));
                }
                else if (segment == "cos")
                {
                    functions.Add(j => j = Math.Cos(j));
                }
                else if (_polynomialPattern.IsMatch(segment))
                {
                    var valueX = GetNumberFromInput(segment.Substring(0, segment.IndexOf('^')));
                    var power = GetNumberFromInput(segment.Substring(segment.IndexOf('^') + 1));

                    functions.Add(j => j = Math.Pow(j, power) * valueX * symbol);
                }
                else if (_simplePattern.IsMatch(segment))
                {
                    var value = GetNumberFromInput(segment);

                    functions.Add(j => j = value * symbol * j);
                }
                else if (_numberPattern.IsMatch(segment))
                {
                    functions.Add(j => j = Convert.ToSingle(segment) * symbol);
                }
                else if (_letterPattern.IsMatch(segment))
                {
                    functions.Add(j => j *= symbol);
                }
                else
                {
                    return;
                }
            }

            for (int i = 0; i < capacity; i++)
            {
                var j = i + min;

                lineX[i] = j;

                var result = 0d;

                foreach (var function in functions)
                {
                    result += function(j);
                }

                lineY[i] = result;
            }

            _dataX = lineX;
            _functions = functions;

            _currentColor = Color.FromArgb(_random.Next(128, 256), _random.Next(128, 256), _random.Next(128, 256));

            RenderPlotScatter(lineX, lineY, $"{text}");   
        }

        private void Scale_Click(object sender, RoutedEventArgs e)
        {
            if(double.TryParse(Error.Text, out var output))
            {
                if(output < 0.01)
                {
                    Precise.IsChecked = false;
                }

                if (output <= 0d)
                {
                    return;
                }
                ScaleTowards(output);
            }        
        }

        private void ScaleTowards(double error)
        {
            CalculatePolynialFromText(Input.Text);

            if (_functions == null || _dataX == null)
            {
                return;
            }

            var scale = 1d;
            var iteration = 0;
            var difference = error + 1;
            var step = 1d;

            while (difference > error)
            {
                (double[], double[]) newScale = ScalePolynomialGraph(_dataX, scale, _functions);

                if(newScale == default)
                {
                    return;
                }

                var (value, fullArea) = CalculateDifference(newScale);
                difference = value;                      

                if (difference < error)
                {
                    var lastCorrectDifference = difference;
                    var lastCorrectX = newScale.Item1;
                    var lastCorrectY = newScale.Item2;
                    var lastCorrectFullArea = fullArea;

                    if (_precise)
                    {
                        var modifier = 1;
                        difference = error + 1;
                        var exit = false;

                        for (var i = 0; i < 15; i++)
                        {
                            modifier = -modifier;
                            step /= 2d;

                            scale += modifier * step;

                            if (scale <= 0)
                            {
                                break;
                            }

                            while (difference > error)
                            {
                                newScale = ScalePolynomialGraph(_dataX, scale, _functions);

                                if (newScale == default)
                                {
                                    return;
                                }

                                (value, fullArea) = CalculateDifference(newScale);
                                difference = value;

                                scale += modifier * step;

                                if (scale <= 0)
                                {
                                    exit = true;
                                    break;
                                }
                            }

                            if (exit)
                            {
                                break;
                            }
                            else
                            {
                                lastCorrectDifference = difference;
                                lastCorrectX = newScale.Item1;
                                lastCorrectY = newScale.Item2;
                                lastCorrectFullArea = fullArea;
                            }

                            if (i == 14) break;
                            difference = error + 1;
                        }
                    }              

                    RenderStepPlot(lastCorrectX, lastCorrectY, $"S: {lastCorrectFullArea} [u^2]; Error: {lastCorrectDifference} ({lastCorrectX.Length} points)");
                    RenderFinalBorder(lastCorrectX[0], lastCorrectX.Last(), lastCorrectY[0], lastCorrectY.Last());
                    return;
                }          

                iteration += 1;
                scale += step;
            }
        }

        private (double[], double[]) ScalePolynomialGraph(double[] lineX, double scale, List<Func<double, double>> calculations)
        {
            int difference;

            try
            {
                difference = checked((int)((lineX.Length - 1) * scale));
            }
            catch
            {
                return default;
            }

            var step = (float)(lineX.Length - 1) / difference;

            var rawY = new double[difference + 1];
            var rawX = new double[difference + 1];

            for (var i = 0; i < difference + 1; i++)
            {
                rawX[i] = lineX[0] + step * i;

                var result = 0d;

                foreach (var calculation in calculations)
                {
                    result += calculation(rawX[i]);
                }

                rawY[i] = result;
            }

            return (rawX, rawY);
        }

        private (double, double) CalculateDifference((double[], double[]) data)
        {
            var (dataX, dataY) = data;

            if (dataX.Length == 2)
            {
                return (double.MaxValue, 0);        
            }

            var fullArea = 0d;
            var result = 0d;
            var lastArea = 0d;
            
            for(var i = 0; i < dataX.Length - 1; i++)
            {
                var a = Math.Abs(dataX[i + 1] - dataX[i]);
                var b = Math.Abs(dataY[i]);

                var area = a * b;
                fullArea += area;

                var difference = Math.Abs(area - lastArea);

                lastArea = area;

                if(difference > result)
                {
                    result = difference;
                }
            }

            return (result, fullArea);
        }

        private void ClearSketch()
        {
            MainGraph.plt.Clear();
            MainGraph.Render();
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            ClearSketch();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            _precise = true;
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            _precise = false;
        }

        private void AutoClear_Checked(object sender, RoutedEventArgs e)
        {
            _autoClearing = true;
        }

        private void AutoClear_Unchecked(object sender, RoutedEventArgs e)
        {
            _autoClearing = false;
        }
    }
}
